# 程序员词典

### 介绍
是一款简单的词典

### 软件架构

>建议：
>
>1. 咱们一起思考有没有更好的解决方案、和框架改进，在以下基础上进行修改
>2. 建议每隔开发一个阶段:互相分享自己的模块、技术、遇到的困难时什么解决的等等..

### 技术选型

#### 后端技术

| Tec                 | Desc                | Office                                         | isDone         |
| ------------------- | ------------------- | ---------------------------------------------- | -------------- |
| SpringBoot          | 容器+MVC框架        | https://spring.io/projects/spring-boot         | √              |
| SpringCloudAlibaba  | 微服务解决方案      | https://spring.io/projects/spring-cloud        | √              |
| Dubbo               | RPC远程调用         | https://dubbo.apache.org/zh/                   | √              |
| Nacos               | 注册中心            | https://nacos.io/zh-cn/index.html              | √              |
| MyBatis             | ORM框架             | http://www.mybatis.org/mybatis-3/zh/index.html | √              |
| Elasticsearch7.14.2 | 搜索引擎            | https://github.com/elastic/elasticsearch       | 已部署，待集成 |
| Redis               | 分布式缓存          | https://redis.io/                              | √              |
| Mysql8.0            | 数据库              | https://www.mysql.com/                         | √              |
| Kibina              | 日志可视化查看工具  | https://github.com/elastic/kibana              | 已部署，待集成 |
| Docker              | 应用容器引擎        | https://www.docker.com/                        | √              |
| Druid               | 数据库连接池        | https://github.com/alibaba/druid               | √              |
| JWT                 | JWT登录支持         | https://github.com/jwtk/jjwt                   | 待集成         |
| Lombok              | 简化对象封装工具    | https://github.com/rzwitserloot/lombok         | 待集成         |
| Hutool              | Java工具类库        | https://github.com/looly/hutool                | √              |
| PageHelper          | MyBatis物理分页插件 | http://git.oschina.net/free/Mybatis_PageHelper | 待集成         |
| Smart-doc           | 文档生成工具        | https://gitee.com/smart-doc-team/smart-doc     | 已集成，待使用 |
| TODO..              |                     |                                                |                |

#### 前端技术

| 技术    | 说明         | 官网                           |
| ------- | ------------ | ------------------------------ |
| Vue     | 前端框架     | https://vuejs.org/             |
| Element | 前端UI框架   | https://element.eleme.io/      |
| Axios   | 前端HTTP框架 | https://github.com/axios/axios |

#### 基础环境

| Tec          | Desc       |
| ------------ | ---------- |
| IDEA         | 开发工具   |
| PostMan      | 接口调用   |
| Maven（3.0+) | 依赖管理   |
| Jdk8         | 开发工具包 |



### 规范

1. 工具可以使用hutool + 咱们统一项目专门写工具类
2. 入参出参不能使用Map、使用DTO、DO、basicModel等
3. 每个类、方法必须有注释（建议多写，把自己的思路写上去）

> 咱们一起维护。相当于一个开发规范


### 安装教程

> 参考此处:https://gitee.com/AcuteSZW/programmer-dictionary/blob/xiGua/javaCode/README.md



