# 后端项目

## 当前进度

1. 微服务(模块)划分
   - dict-user
   - dict-trade
   - dict-store
   - dict-gateway
   - 公共包 dict-common
   - 公共RPC接口 dict-api
2. 技术栈
   - SpringCloudAlibaba (基础框架)
   - Gateway （网关）
   - Nacos (注册中心)
   - Dubbo （远程调用）
   - Mybatis （ORM框架）
   - Redis
   - ...



## 快速开始

### 1 环境准备

| Name         | Desc                                          |
| ------------ | --------------------------------------------- |
| IDEA开发工具 |                                               |
| JDK1.8       |                                               |
| Nacos        | 下载到本地，项目启动时，需要**首先启动**Nacos |
| Redis        | 下载到本地，项目启动时，需要**首先启动**Redis |

#### 1.1 Nacos

>下载方式：
>
>1. [官网](https://github.com/alibaba/nacos/tags)(下载1.3.1版本的zip文件，下载比较慢)
>2. [百度云](https://pan.baidu.com/s/1Jg8tb8uyQFrD7YHuuFwZgw)(提取码：e5lc)

启动

1. 修改 startup.cmd 内容为单机启动

   ```
   set MODE=“cluster 替换为 set MODE=“standalone
   ```

2. 访问 [Nacos仪表盘](http://localhost:8848/nacos/#/)



#### 1.2 Redis

[Redis入门、安装参考此处](https://hopestation.top/archives/redis1)



### 2 项目启动



2. 启动Nacos

2. 构建Maven项目，Import依赖

3. 启动dict-gateway、dict-user、dict-store、dict-trade
   ![image-20211128220708262](README.assets/image-20211128220708262.png)

4. 访问[Nacos仪表盘](http://localhost:8848/nacos/#/),出现一下服务则表示启动成功

   ![image-20211127141632873](README.assets/image-20211127142146859.png)

5. 访问[测试路径](http://localhost:8899/dict-user/test/hello)，出现下图则表示访问成功
   ![image-20211127141632873](README.assets/image-20211127141632873.png)



