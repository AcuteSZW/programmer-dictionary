package top.easydict.log;

import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;

/**
 * 做一个环绕通知
 */
@Aspect
@Component
public class InterfaceLog {

    //创建日志对象
    private Logger logger = LoggerFactory.getLogger(InterfaceLog.class);

    //定义一个切点  controller下所有类,类下所有方法,方法里面所有参数(*.*)
    @Pointcut(value = "execution(* top.easydict.*.online.controller.*.*(..))")
    public void myPointcut(){

        Executors.newFixedThreadPool(100);
    }
    //定义通知
    @Around("myPointcut()")
    public Object myLogger(ProceedingJoinPoint point) throws Throwable {

        //获取类
        String className = point.getTarget().getClass().toString();
        //获取方法
        String medthod = point.getSignature().getName();
        //获取参数
        Object[] objects = point.getArgs();
        ObjectMapper mapper = new ObjectMapper();
        logger.info("传参前获取的参数为"+className+" : "+medthod+"传递的参数为:"+mapper.writeValueAsString(objects));
        //执行目标函数
        Object obj = point.proceed();
        //打印结果集
        logger.info("执行后获取的参数为"+className+" : 方法: "+medthod+"()传递的参数为:"+mapper.writeValueAsString(obj));
        return obj;
    }

}
