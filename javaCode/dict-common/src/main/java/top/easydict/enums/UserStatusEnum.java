package top.easydict.enums;

/**
 * @Author: liangfan
 * @Date: 2021-12-20 09:38
 * @Description:
 */
public enum UserStatusEnum {
    /**
     *
     */
    FROZEN(0, "冻结"),
    ACTIVE(1, "激活"),
    DELETED(2, "注销");

    private final Integer code;
    private final String info;

    UserStatusEnum(Integer code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public Integer getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
