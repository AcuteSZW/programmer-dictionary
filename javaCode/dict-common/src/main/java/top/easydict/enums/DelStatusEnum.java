package top.easydict.enums;

/**
 * @Author: liangfan
 * @Date: 2021-12-20 14:06
 * @Description:
 */
public enum DelStatusEnum {
    /**
     *
     */
    NOTDEL(0, "未删除"),
    DEL(1, "已删除");

    private final Integer code;
    private final String info;

    DelStatusEnum(Integer code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public Integer getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
