package top.easydict.util.jwt.entity;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * 自定义的shiro接口token，可以通过这个类将string的token转型成AuthenticationToken，可供shiro使用
 * @Author: north
 * Date:2021/12/5 16:16
 */

public class JwtToken implements AuthenticationToken {
    private String token;

    public JwtToken() {
    }

    public JwtToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
