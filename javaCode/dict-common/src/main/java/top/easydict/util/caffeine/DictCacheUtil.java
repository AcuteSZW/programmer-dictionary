package top.easydict.util.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.concurrent.TimeUnit;

/**
 * 本地换成util
 */
public class DictCacheUtil {

	private static Cache<String, Object> cache;

	static {
		cache = createCache();
	}

	/**
	 * 插入缓存 | 更新缓存
	 * @param cacheName 缓存前缀
	 * @param key 键
	 * @param value 值
	 */
	public static void saveCache(String cacheName, String key, Object value) {
		System.out.printf("存储缓存的数据 key（%s） value（%s）",cacheName + "-" + key, value);
		System.out.println();
		cache.asMap().put(cacheName + "-" + key, value);
		cache.put(key,value);
		System.out.println("已存入缓存");
	}

	/**
	 * 根据键获取缓存
	 * @param cacheName 缓存前缀
	 * @param key 键
	 * @return Object类型, 由使用者自己转换
	 */
	public static Object getCache(String cacheName, String key) {
		System.out.printf("获取缓存的数据 key（%s） value（%s）",cacheName + "-" + key, cache.asMap().get(cacheName + "-" + key) );
//		System.out.printf("获取缓存的数据 key（%s） value（%s）",cacheName + "-" + key, cache.getIfPresent(key));
		System.out.println();
//		return cache.getIfPresent(key);
		return cache.asMap().get(cacheName + "-" + key);
	}

	/**
	 * 根据键值删除缓存
	 * @param cacheName 缓存前缀
	 * @param key 建
	 */
	public static void deleteCache(String cacheName, String key) {
		System.out.printf("删除缓存的数据 key（%s）", cacheName + "-" + key);
		System.out.println();
		cache.asMap().remove(cacheName + "-" + key);
	}

	private static class CacheSingletonHolder {
		private final static Cache<String, Object> CACHE = Caffeine.newBuilder()
				// 初始的缓存空间大小
				.initialCapacity(100)
				// 缓存的最大条数
				.maximumSize(1000)
				// 最后一次缓存访问过后,7天后失效
				.expireAfterAccess(1000, TimeUnit.MILLISECONDS)
				.build();
	}
	 private static Cache<String, Object> createCache() {
		return CacheSingletonHolder.CACHE;
	}
}