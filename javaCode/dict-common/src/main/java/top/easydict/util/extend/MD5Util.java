package top.easydict.util.extend;

import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;

/**
 * @Author: liangfan
 * @Date: 2021-12-20 09:38
 * @Description: MD5
 */
public class MD5Util {
    /**
     * 生成含有随机盐的密码
     */
    public static String generate(String password, String salt) {
        password = md5Hex(password + salt);
        return password;
    }

    /**
     * 获取十六进制字符串形式的MD5摘要
     */
    public static String md5Hex(String src) {
        System.out.println(src);
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] bs = md5.digest(src.getBytes());
            return new String(new Hex().encode(bs));
        } catch (Exception e) {
            return null;
        }
    }
}  