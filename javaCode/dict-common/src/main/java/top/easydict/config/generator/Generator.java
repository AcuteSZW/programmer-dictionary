package top.easydict.config.generator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;


import java.util.Collections;

/**
 * @Author: liangfan
 * @Date: 2021-12-06 14:36
 * @Description: mybatis-plus自动生成，之后调整文件位置
 */

public class Generator {
    public static void main(String[] args) {
        FastAutoGenerator.create(
                        "jdbc:mysql://150.158.77.92:3306/dict?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true",
                        "poster",
                        "dictposter")
                .globalConfig(builder -> {
                    builder.author("xxx") // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .enableSwagger() // 开启 swagger 模式
                            // 指定输出路径
                            // 直接右键复制项目根目录的绝对路径
                            // 例如 E:\IdeaProject\programmer-dictionary\javaCode\dict-user\src\main\java
                            .outputDir("xxx");
                })
                .packageConfig(builder -> {
                    builder.parent("top.easydict.config.generator.generatorFiles") // 包路径
                            // 设置mapperXml生成路径
                            // 直接右键复制项目mapper文件夹的绝对路径
                            // 例如 E:\IdeaProject\programmer-dictionary\javaCode\dict-user\src\main\java\top\easydict\config\generator\generatorFiles
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml,"xxx"));
                })
                .strategyConfig(builder -> {
                    builder.addInclude("xxx") // 需要生成的表名
                            .addTablePrefix("dict_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

}
