package top.easydict.tasktime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务
 */
@Component
@Async
@Configuration
@EnableScheduling
public class TaskTime {
    private Logger logger = LoggerFactory.getLogger(TaskTime.class);

    @Scheduled(fixedDelay = 1000*30)
    public void doTask(){
        logger.info("定时任务");
    }
}
