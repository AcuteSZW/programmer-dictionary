package top.easydict.service;

import java.util.Map;

/**
 * @Desc: User服务远程调用统一规范
 * @Author HopeStation
 * At 2021/11/28 20:17
 */
public interface UserServiceRemote {

    /**
     * 查询用户信息列表
     * @param param
     * @return
     */
    public Map<String,Object> queryUserList(Map<String,Object> param);

    /**
     * 根据用户ID查询用户
     * @param userId
     * @return
     */
    public Map<String,Object> findUserById(String userId);

}
