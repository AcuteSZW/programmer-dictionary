package top.easydict.servicecode;

/**
 * 用户模块的服务码
 */
public class DictUserServiceCode {
    /** 登录的服务码 */
    public static final String LOGIN = "user.login";
    /** 注册,添加用户 */
    public static final String ADDUSER = "user.add";
}
