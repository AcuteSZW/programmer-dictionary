package top.easydict;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Desc: 交易服务启动类
 *  - 交易服务包括：订单、支付等功能
 * @Author HopeStation
 * At 2021/11/27 2:11
 */
@SpringBootApplication
public class DictTradeApplication {
    public static void main(String[] args) {
        SpringApplication.run(DictTradeApplication.class,args);
    }
}
