package top.easydict;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @Desc: 用户服务启动类
 * @Author HopeStation
 * At 2021/11/27 2:11
 */
@EnableDubbo
@EnableAsync
@MapperScan("top.easydict.user.domain.dao")
@SpringBootApplication
public class DictUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(DictUserApplication.class,args);
    }
}
