package top.easydict.user.online.controller;

import top.easydict.user.online.dto.UserRequestDTO;
import top.easydict.user.online.dto.UserResponseDTO;

public interface UserService {
    /**
     * 测试
     * @return
     */
    public String helloWorld();

    /**
     * 访问添加用户
     */
    public UserResponseDTO insertUser(UserRequestDTO userRequestDTO);
}
