package top.easydict.user.online.conversion;

import cn.hutool.core.bean.BeanUtil;
import lombok.val;
import top.easydict.user.domain.entity.UserDO;
import top.easydict.user.online.dto.UserRequestDTO;
import top.easydict.user.online.dto.UserResponseDTO;

/**
 * 这里是做DTO转DO的
 */
public class UserConversion {

    /**
     *  将DTO的入参转为DO的操作参数
     *
     * @param userRequestDTO
     * @return
     */
    public static UserDO userConversionDO(UserRequestDTO userRequestDTO){
        UserDO userDO = BeanUtil.copyProperties(userRequestDTO, UserDO.class);

        return userDO;
    }

    public static UserResponseDTO userResponseDTO(UserDO userDO){
        UserResponseDTO userResponseDTO = BeanUtil.copyProperties(userDO, UserResponseDTO.class);

        return userResponseDTO;
    }
}
