package top.easydict.user.domain.innerservice.remote;

import org.apache.dubbo.config.annotation.DubboService;
import top.easydict.service.UserServiceRemote;

import java.util.HashMap;
import java.util.Map;

/**
 * @Desc: User服务远程调用实现类
 * @Author HopeStation
 * At 2021/11/28 20:55 实现
 */
@DubboService
public class UserServiceRemoteImpl implements UserServiceRemote {

    @Override
    public Map<String, Object> queryUserList(Map<String, Object> param) {
        return null;
    }

    @Override
    public Map<String, Object> findUserById(String userId) {

        //TODO 返回值的转换需要设计一下，最好不能一直是Map： 1. 统一返回对象进行转换 2.Map 3， json化
        //模拟查询数据库 返回User对象信息
        Map user = new HashMap();
        user.put("id",userId);
        user.put("name","zhangSan");
        user.put("age","11");
        return user;
    }
}
