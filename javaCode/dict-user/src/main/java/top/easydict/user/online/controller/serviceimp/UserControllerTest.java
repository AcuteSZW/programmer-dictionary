package top.easydict.user.online.controller.serviceimp;

import org.springframework.web.bind.annotation.*;
import top.easydict.user.online.dto.UserRequestDTO;

/**
 * @Desc: 用户功能API类
 * @Author HopeStation
 * At 2021/11/27 2:23
 */
@RestController
@RequestMapping("user")
public class UserControllerTest {


    @PostMapping("/pwdLogin")
    public String pwdLogin(@RequestBody UserRequestDTO userDTO){
        //TODO 统一封装返回对象、返回类型、状态码
        return isValidUser(userDTO)?"Login OK":"Login Failed";
    }

    private boolean isValidUser(UserRequestDTO userDTO){
        //TODO 后续使用 JWT + Redis. 结合网关进行鉴权
        //TODO 结合数据库
        if( "123".equals(userDTO.getPassword()) && "root".equals(userDTO.getUsername()) ){
            return true;
        }
        return false;
    }

}
