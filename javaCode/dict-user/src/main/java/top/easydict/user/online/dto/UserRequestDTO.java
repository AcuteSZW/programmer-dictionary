package top.easydict.user.online.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Desc: 用户类视图交互对象
 * @Author HopeStation
 * At 2021/11/27 2:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDTO {
    private String username;
    private String password;
    private String email;
    private String phone;
    private String status;
    //功能码
    private String funcode;
}
