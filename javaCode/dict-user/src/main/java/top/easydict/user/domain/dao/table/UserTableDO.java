package top.easydict.user.domain.dao.table;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 数据库接收对象
 */
@Data
@TableName("dict_user")
public class UserTableDO implements Serializable {

    @TableId(value="user_id",type= IdType.AUTO)
    private long userId;
    private String userName;
    private String password;
    private String roleName;
    private String phone;
    private String email;
    private long status;
    private Timestamp createTime;
    private Timestamp updateTime;

    @TableLogic
    @TableField(fill = FieldFill.INSERT_UPDATE,select = false)//自动填充，影响插入和更新,select = false查询忽略
    private Integer isDelete;
}
