package top.easydict.user.domain.innerservice;

import top.easydict.user.domain.entity.UserDO;
import top.easydict.user.online.dto.UserRequestDTO;

import java.util.HashMap;
import java.util.Map;

public interface UserServices {
    public UserDO addUser(UserDO userDO);

    /**
     * 注册
     * @param userRequestDTO
     * @return
     */
    public Map register(UserRequestDTO userRequestDTO);

    /**
     * 校验注册信息
     * @param username
     * @param email
     * @param phone
     * @return
     */
    public Map checkRegisterInfo(String username, String email, String phone);

    /**
     * 校验验证码
     * @param userId
     * @param code
     * @return
     */
    public Map checkRegisterCode(Long userId, String code);
}
