package top.easydict.user.domain.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.easydict.user.domain.dao.table.UserTableDO;

/**
 * mybatis提供简单的增删改查
 */
@Mapper
public interface UserMapper extends BaseMapper<UserTableDO> {
    Integer insertUser(UserTableDO user);
}
