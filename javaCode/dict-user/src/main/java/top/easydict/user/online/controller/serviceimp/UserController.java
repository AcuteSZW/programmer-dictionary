package top.easydict.user.online.controller.serviceimp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.easydict.servicecode.DictUserServiceCode;
import top.easydict.user.domain.entity.UserDO;
import top.easydict.user.domain.innerservice.UserServices;
import top.easydict.user.online.controller.UserService;
import top.easydict.user.online.conversion.UserConversion;
import top.easydict.user.online.dto.UserRequestDTO;
import top.easydict.user.online.dto.UserResponseDTO;

import java.util.Map;

/**
 * @Desc: 测试API类
 * @Author HopeStation
 * At 2021/11/27 2:23
 */
@RestController
@RequestMapping
@Api(tags = "用户管理")
public class UserController implements UserService {
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserServices userServices;

    @GetMapping(DictUserServiceCode.LOGIN)
    public String helloWorld(){
        logger.info("access /test/hello");
        return "程序员字典项目：【DICT-USER】访问正常";
    }

    @Override
    @RequestMapping(path = DictUserServiceCode.ADDUSER,method = RequestMethod.POST)
    public UserResponseDTO insertUser(UserRequestDTO userRequestDTO) {
        UserDO userDO = userServices.addUser(UserConversion.userConversionDO(userRequestDTO));
        return UserConversion.userResponseDTO(userDO);
    }


    /**
     * 注册 http://localhost:6903/swagger-ui/index.html#/
     * @param userDTO
     * @return
     */
    @ApiOperation(value = "用户注册")
    @PostMapping("/register")
    public Map register(UserRequestDTO userDTO){
        return userServices.register(userDTO);
    }

    /**
     * 校验（注册）
     * @param username
     * @param email
     * @param phone
     * @return
     */
    @ApiOperation(value = "用户参数校验")
    @PostMapping("/checkRegisterInfo")
    public Map checkRegisterInfo(String username, String email, String phone){
        return userServices.checkRegisterInfo(username, email, phone);
    }

    /**
     * 校验验证码并激活账号
     * @param userId
     * @param code
     * @return
     */
    @ApiOperation(value = "校验验证码并激活账号")
    @PostMapping("/checkRegisterCode")
    public Map checkRegisterCode(Long userId, String code){
        return userServices.checkRegisterCode(userId, code);
    }

}
