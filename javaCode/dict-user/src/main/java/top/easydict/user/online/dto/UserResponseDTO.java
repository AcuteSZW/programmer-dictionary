package top.easydict.user.online.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Desc: 用户类视图交互对象
 * @Author HopeStation
 * At 2021/11/27 2:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponseDTO {
    //TODO 这里可以结合Lombok 或者 HUTool.自动生成Getter\Setter
    private String username;
    private String password;
    private String messge;
}
