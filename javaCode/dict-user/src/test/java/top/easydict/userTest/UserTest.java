package top.easydict.userTest;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.easydict.DictUserApplication;
import top.easydict.user.domain.dao.table.UserTableDO;
import top.easydict.user.domain.dao.UserMapper;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DictUserApplication.class)
public class UserTest {

    @Autowired
    private UserMapper userService;

    /**
     * 测试向服务器数据库插入一条数据 虚假数据
     */
    @Test
    public void te0(){
        UserTableDO userTableDO = new UserTableDO();
        userTableDO.setEmail("1296612332@qq.com");
        userTableDO.setUserName("小李1");
        userTableDO.setStatus(1);
        userService.insert(userTableDO);
    }

    //逻辑删除
    @Test
    public void te1(){
        QueryWrapper<UserTableDO> user = new QueryWrapper<>();
        user.eq("user_id",16);
        userService.delete(user);
    }

    @Test
    public void te2(){
        //mybatis plus的条件构造器
        LambdaQueryWrapper<UserTableDO> wrapper = new LambdaQueryWrapper<>();
        UserTableDO user = new UserTableDO();
        user.setUserName("小李");
        wrapper.eq(StringUtils.isNotBlank(user.getUserName()), UserTableDO::getUserName, user.getUserName());
        wrapper.eq(user.getUserId() != 0,UserTableDO::getUserId,user.getUserId());
        UserTableDO userList = userService.selectOne(wrapper);
        String toJsonStr = JSONUtil.toJsonStr(userList);
        System.out.println(toJsonStr);
    }
}
