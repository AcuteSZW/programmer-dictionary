package top.easydict.email;

import cn.hutool.extra.mail.MailUtil;
import org.junit.jupiter.api.Test;

public class TestEmail {

    /**
     * 测试邮件发送
     *  - 注意：请利用自己的邮箱进行测试! 请勿使用他人邮箱或者随机邮箱!
     */
    @Test
    public void sendEmail(){
        //测试后请勿提交代码，本地保存即可
        String yourSelfMailAddress = "请使用自己的邮箱地址进行测试：例如 myaddress@xx.com";
        String mailSubject = "测试邮件主题";
        String mailContent = "测试邮件内容";
        MailUtil.send(yourSelfMailAddress, mailSubject, mailContent, false);
    }
}
