package top.easydict.online.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.easydict.service.UserServiceRemote;

import java.util.Map;

/**
 * @Desc: 词典仓库服务
 * @Author HopeStation
 * At 2021/11/28 20:54
 */
@RestController
@RequestMapping("/store")
public class StoreController {

    @DubboReference
    private UserServiceRemote userServiceRemote;

    /**
     * 模拟查询词典、用户数据
     * @return
     */
    @GetMapping("/findStoreAndUser")
    public String findStoreAndUser(){
        Map<String, Object> userObj = userServiceRemote.findUserById("1234");
        return "Store is testStore,And User is " + userObj;
    }

}
