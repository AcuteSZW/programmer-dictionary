package top.easydict;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Desc: 字典仓库服务启动类
 *  - 提供字典查询、分类等功能
 * @Author HopeStation
 * At 2021/11/27 2:11
 */
@EnableDubbo
@SpringBootApplication
public class DictStoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(DictStoreApplication.class,args);
    }
}
