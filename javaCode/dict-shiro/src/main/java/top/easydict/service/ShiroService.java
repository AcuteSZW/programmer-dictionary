package top.easydict.service;

import top.easydict.entity.RoleDO;
import top.easydict.entity.RoleResourceDO;
import top.easydict.entity.UserDO;

import java.util.List;

/**
 * @Author: north
 * Date:2021/12/5 17:14
 */
public interface ShiroService {
    UserDO getUserByName(String username);

    String getPassword(String username);

    String getUserStatus(String username);

    List<RoleDO> getUserRole(String username);

    List<RoleResourceDO> getUserRoleReasource(String username);
}
