package top.easydict.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.easydict.entity.RoleDO;
import top.easydict.entity.RoleResourceDO;
import top.easydict.entity.UserDO;
import top.easydict.mapper.ShiroMapper;
import top.easydict.service.ShiroService;

import java.util.List;

/**
 * @Author: north
 * Date:2021/12/5 17:13
 */
@Service
public class ShiroDomainService implements ShiroService {
    @Autowired
    ShiroMapper userMapper;

    @Override
    public UserDO getUserByName(String username) {
        return userMapper.getUserDO();
    }

    @Override
    public String getPassword(String username) {
        return userMapper.getPassword(username);
    }

    @Override
    public String getUserStatus(String username) {
        return userMapper.getUserStatus(username);
    }

    @Override
    public List<RoleDO> getUserRole(String username) {
        return userMapper.getUserRole(username);
    }

    @Override
    public List<RoleResourceDO> getUserRoleReasource(String username) {
        return userMapper.getUserRoleReasource(username);
    }
}
