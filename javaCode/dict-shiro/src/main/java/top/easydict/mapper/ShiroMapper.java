package top.easydict.mapper;

import org.springframework.stereotype.Repository;
import top.easydict.entity.RoleDO;
import top.easydict.entity.RoleResourceDO;
import top.easydict.entity.UserDO;

import java.util.ArrayList;
import java.util.List;

/**
 * 模拟用户数据
 * @Author: north
 * Date:2021/12/5 21:50
 */
@Repository
public class ShiroMapper {
    UserDO userDO;

    ShiroMapper() {
        this.userDO = this.getUserDO();
    }

    public UserDO getUserDO() {
        UserDO userDO = new UserDO();
        userDO.setUserName("jammy");
        userDO.setPassWord("123456");
        RoleDO roleDO = new RoleDO();
        List<RoleResourceDO> roleResources = new ArrayList<>();
        roleResources.add(new RoleResourceDO("系统管理"));
        roleResources.add(new RoleResourceDO("用户管理"));
        roleDO.setRoleName("管理员");
        roleDO.setRoleResources(roleResources);
        List<RoleDO> roleDOList = new ArrayList<>();
        roleDOList.add(roleDO);
        userDO.setRoleDOList(roleDOList);
        userDO.setStatus("0");
        return userDO;
    }

    public List<RoleDO> getUserRole(String username) {
        return this.userDO.getRoleDOList();
    }

    public List<RoleResourceDO> getUserRoleReasource(String username) {
        List<RoleDO> roleDOList = this.userDO.getRoleDOList();
        List<RoleResourceDO> roleResources = new ArrayList<>();
        for (RoleDO roleDO : roleDOList) {
            for (RoleResourceDO roleResourceDO : roleDO.getRoleResources()) {
                roleResources.add(roleResourceDO);
            }
        }

        return roleResources;
    }

    public String getUserStatus(String username) {
        return this.userDO.getStatus();
    }

    public String getPassword(String username) {
        return this.userDO.getPassWord();
    }
}
