package top.easydict.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: north
 * Date:2021/12/5 21:50
 */
@Data
public class UserReqDTO implements Serializable{
        private String username;
        private String password;
}
