package top.easydict.entity;

import lombok.Data;

import java.util.List;

/**
 * @Author: north
 * Date:2021/12/5 21:50
 */
@Data
public class UserDO {
    private String userName;
    private String passWord;
    private String status;
    private List<RoleDO> roleDOList;

}
