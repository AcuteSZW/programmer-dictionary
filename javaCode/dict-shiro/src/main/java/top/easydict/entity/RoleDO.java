package top.easydict.entity;

import lombok.Data;

import java.util.List;

/**
 * @Author: north
 * Date:2021/12/5 21:50
 */
@Data
public class RoleDO {
    private String roleName;
    private List<RoleResourceDO> roleResources;
}
