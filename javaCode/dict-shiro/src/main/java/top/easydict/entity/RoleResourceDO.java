package top.easydict.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: north
 * Date:2021/12/5 21:50
 */
@Data
@AllArgsConstructor
public class RoleResourceDO {
    private String resourceName;
}
