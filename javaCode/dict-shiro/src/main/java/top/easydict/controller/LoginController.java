package top.easydict.controller;



import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.easydict.dto.UserReqDTO;
import top.easydict.entity.Msg;
import top.easydict.service.ShiroService;
import top.easydict.util.jwt.JwtUtil;

/**
 * @Author: north
 * Date:2021/12/5 21:50
 */
@RestController
@Slf4j
@RequestMapping("/test")
public class LoginController {
    @Autowired
    ShiroService userService;

    @PostMapping("/login")
    public Msg login(UserReqDTO userReqDTO){
        String realPassword = userService.getPassword(userReqDTO.getUsername());
        if (realPassword == null) {
            return Msg.fail().add("info","用户名错误");
        } else if (!realPassword.equals(userReqDTO.getPassword())) {
            return Msg.fail().add("info","密码错误");
        } else {
            return Msg.success().add("token", JwtUtil.createToken(userReqDTO.getUsername()));
        }
    }



    @RequiresRoles("管理员")
    @PostMapping("/admin")
    public String admin() {
        return "Administrator success!";
    }


    @RequiresPermissions("系统管理")
    @PostMapping("/manage")
    public String manage() {
        return "系统管理 success!";
    }


    @RequiresPermissions("个人设置")
    @PostMapping("/setting")
    public String setting() {
        return "个人设置 success!";
    }
}
