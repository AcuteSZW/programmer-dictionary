package top.easydict.config;


import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.easydict.entity.RoleDO;
import top.easydict.entity.RoleResourceDO;
import top.easydict.entity.UserDO;
import top.easydict.service.ShiroService;
import top.easydict.util.jwt.JwtUtil;
import top.easydict.util.jwt.entity.JwtToken;

/**
 * @Author: north
 * Date:2021/12/5 21:50
 */
@Component
public class CustomRealm extends AuthorizingRealm {

    @Autowired
    private ShiroService userService;

    /**
     * 必须重写此方法，不然会报错
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 默认使用此方法进行用户名正确与否验证，错误抛出异常即可。
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("————身份认证方法————");
        String token = (String) authenticationToken.getCredentials();
        // 解密获得username，用于和数据库进行对比
        String username = JwtUtil.getUsername(token);
        if (username == null || !JwtUtil.verify(token, username)) {
            throw new AuthenticationException("token认证失败！");
        }

        UserDO user=userService.getUserByName(username);
        if (user == null) {
            throw new AuthenticationException("该用户不存在！");
        }
        String ban = user.getStatus();
        if ("1".equals(ban)) {
            throw new AuthenticationException("该用户已被封号！");
        }

        return new SimpleAuthenticationInfo(token, token, "MyRealm");
    }

    /**
     * 只有当需要检测用户权限的时候才会调用此方法，例如checkRole,checkPermission之类的
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        System.out.println("————权限认证————");
        String username = JwtUtil.getUsername(principals.toString());
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //获得该用户
        UserDO user=userService.getUserByName(username);

        //给用户设置角色
        for (RoleDO role:user.getRoleDOList()){
            info.addRole(role.getRoleName());
            //给用户设置权限
            for (RoleResourceDO resource:role.getRoleResources()){
                info.addStringPermission(resource.getResourceName());
            }
        }

        return info;
    }
}
