package top.easydict;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @Author: north
 * Date:2021/12/5 20:16
 */
@EnableDubbo
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class DictShiroApplication {
    public static void main(String[] args) {
        SpringApplication.run(DictShiroApplication.class,args);
    }
}
