package top.easydict;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Desc: 统一网关服务
 *  - 提供鉴权、限流、路由转发等功能
 * @Author HopeStation
 * At 2021/11/27 2:11
 */
@SpringBootApplication
public class DictGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(DictGatewayApplication.class,args);
    }
}
