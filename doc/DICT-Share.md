

# 技术分享





### TODO

| Stack                | Desc                                                         | progress                               |
| -------------------- | ------------------------------------------------------------ | -------------------------------------- |
| ElasticSearch        | 由于词典业务，存在海量数据搜索场景，ES必不可少。考虑Mysql、ES多实现的方案来编写样例 | 服务器安装成功，下一步集成到springboot |
| Hystrix              | 在测试代码中Dubbo调用存在超时的情况，需要进行熔断、降级处理  |                                        |
| kola-cloud           | 服务监控功能，暂时没想好用什么                               |                                        |
| Token                | 登录认证机制，正在考虑解决方案。结合网关\|缓存来实现         |                                        |
| Mysql8(~~Mysql5.7~~) | 数据库                                                       |                                        |
| Docker               | 中间件尽量用Docker来部署                                     |                                        |
| Jekins               | 项目成型后考虑自动化部署                                     |                                        |
| Cache                | 本地缓存                                                     |                                        |
| HuTool               | 工具包                                                       |                                        |



### 团队开发

- 使用Gitee|GitHub:创建组织|organization，假如成员一起开发
- 使用Gitee|GitHub:创建仓库后邀请其他成为，设置为开发者角色共同开发
- 使用GitLab企业级功能等



### 创建微服务

> 实际创建方式不只此一种 | [参考视频](https://www.bilibili.com/video/BV1764y1676c?from=search&seid=10738455620982589161&spm_id_from=333.337.0.0) 

1. 使用IDEA：首先创建一个空的`Maven`项目，然后可以把src目录删除

2. 右击项目选择`new module`,再创建N个Maven项目

3. 在最外层的pom，来管理Version

   ```xml
     <properties>
   		your version here
           <spring-cloud.version>Hoxton.SR6</spring-cloud.version>
           <spring.cloud.alibaba.version>2.2.1.RELEASE</spring.cloud.alibaba.version>
           <mysql.connector.java.version>5.1.38</mysql.connector.java.version>
          	....
       </properties>
   
       <dependencyManagement>
           you dependencyManagement here
           <dependencies>
               <!--引入springcloud alibaba-->
               <dependency>
                   <groupId>com.alibaba.cloud</groupId>
                   <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                   <version>${spring.cloud.alibaba.version}</version>
               </dependency>
               ....
           </dependencies>
       </dependencyManagement>
   ```

4. 一般创建一个`xx-common`来作为公共工具包、共同的依赖也可以放在这里

```
一般微服务项目结构：
- you project
	- xx-common
	- xx-serviceA
		- your java source
		- pom.xml
	- xx-serviceB
	- xx-serviceB
	- ...
	pom.xml
	
```





### 创建网关

POM

```xml
<!--gateway-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-gateway</artifactId>
</dependency>

```

YMl

```yml
spring:
  application:
    name: DICT-GATEWAY
  cloud:
    nacos:
      server-addr: 127.0.0.1:8848
    gateway:
      routes: #用来配置路由规则
        # dict-user
        - id: dict-user
          uri: lb://DICT-USER
          predicates:
            - Path=/dict-user/**
          filters:
            - StripPrefix=1   #去掉请求前缀filter    int数字  1 去掉一级   2 去掉两级
        # dict-trade
        - id: dict-trade
          uri: lb://DICT-TRADE
          predicates:
            - Path=/dict-trade/**
          filters:
            - StripPrefix=1   #去掉请求前缀filter    int数字  1 去掉一级   2 去掉两级
        # dict-user
        - id: dict-store
          uri: lb://DICT-STORE
          predicates:
            - Path=/dict-store/**
          filters:
            - StripPrefix=1   #去掉请求前缀filter    int数字  1 去掉一级   2 去掉两级

      globalcors: # 跨域配置处理
        cors-configurations:
          '[/**]':
            allowedOrigins: "*"
            allowedMethods: "*"
            allowedHeaders: "*"
```



### Dubbo远程调用

> [官网参考文档](https://github.com/alibaba/spring-cloud-alibaba/tree/master/spring-cloud-alibaba-examples/spring-cloud-alibaba-dubbo-examples) | [官网参考项目](https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-examples/spring-cloud-alibaba-dubbo-examples/README_CN.md) | [Dubbo&ZooKeeper](https://hopestation.top/archives/dz01)

#### 三个步骤

1. 编写Interface接口
2. 提供者实现Interface接口
3. 消费者调用Interface接口

#### 实际操作

在此项目中我们把`编写Interface接口`这个步骤抽离成了一个单独的模块，然后提供者、消费者引入此目录。这个单独模块叫`dict-api`

1. POM

```xml
<!-- 
            我这里为了使用DubboService\DubboRefrence注解，所以故意引入了2.7.7版本。
            而没有使用 ubbo Spring Cloud Starter 因为目前这个依赖最高支持 2.7.6
            旧版本需要使用与Spring同名称的Service注解，不太合适。新版已经弃用-->
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <artifactId>dubbo-spring-boot-starter</artifactId>
    <version>2.7.7</version>
</dependency>

<!--  为了将Dubbo接口，注册进入Nacos来进行注册与发现。也可以使用ZK  -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>dubbo-registry-nacos</artifactId>
    <version>2.7.7</version>
</dependency>
```

2. YML

   ```yml
   dubbo:
     scan:
       # dubbo 服务扫描基准包
       base-packages: top.easydict.service
     protocol:
       # dubbo 协议
       name: dubbo
       # dubbo 协议端口（ -1 表示自增端口，从 20880 开始）
       port: -1
     registry:
       # 挂载到 Spring Cloud 注册中心 此处需要dubbo-registry-nacos依赖包支持
       address: nacos://127.0.0.1:8848
   	# 配置了启动时暂不扫描接口，好像失效了，暂时没查出原因
       check: false
     cloud:
       subscribed-services: dict-user; dict-store
   ```

3. 启动类

   ```java
   //增加此注解
   @EnableDubbo
   @SpringBootApplication
   public class DictUserApplication {
       public static void main(String[] args) {
           SpringApplication.run(DictUserApplication.class,args);
       }
   }
   ```

4. api项目中增加接口，对应此步骤`1.编写Interface接口`

   ```java
   public interface UserServiceRemote {
       /**
        * 根据用户ID查询用户
        * @param userId
        * @return
        */
       public Map<String,Object> findUserById(String userId);
   
   }
   
   ```

5. 提供者增加接口实现类,对应此步骤`2.提供者实现Interface接口`

   ```java
   @DubboService
   public class UserServiceRemoteImpl implements UserServiceRemote {
       @Override
       public Map<String, Object> findUserById(String userId) {
           // init user
           return user;
       }
   }
   
   ```

6. 消费者调用接口，对应此步骤`3.消费者调用Interface接口`

   ```java
   @RestController
   @RequestMapping("/store")
   public class StoreController {
   
       @DubboReference
       private UserServiceRemote userServiceRemote;
   
       /**
        * 模拟查询词典、用户数据
        * @return
        */
       @GetMapping("/findStoreAndUser")
       public String findStoreAndUser(){
           // userServiceRemote.findUserById 为Dubbo远程调用
           Map<String, Object> userObj = userServiceRemote.findUserById("1234");
           return "Store is testStore,And User is " + userObj;
       }
   
   }
   ```

   

#### 测试

按如下顺序启动项目：

1. dict-gateway
2. dict-user
3. dict-store

访问路径：http://localhost:8899/dict-store/store/findStoreAndUser 

1. 出现以下结果则为Dubbo远程调用成功。
   ![image-20211128230439921](DICT-Share.assets/image-20211128230439921.png)

2. 同时Nacos的服务列表也会出现两个服务，实际上是Dubbo的接口

![image-20211128231221180](DICT-Share.assets/image-20211128231221180.png)

3. 建议Debug调试dict-user\dict-store服务的接口处，来观察情况



### Redis

1. POM

```xml
<!--redis starter -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<!--jackson 序列化包 -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.9.8</version>
</dependency>
```

2. yml

   ```yml
   redis:
     # Redis数据库索引（默认为0）
     database: 0
     # Redis服务器地址
     host: localhost
     # Redis服务器连接端口
     port: 6379
     pool:
       # 连接池最大连接数（使用负值表示没有限制）
       max-active: 8
       # 连接池最大阻塞等待时间（使用负值表示没有限制）
       max-wait: -1
       # 连接池中的最大空闲连接
       max-idle: 8
       # 连接池中的最小空闲连接
       min-idle: 0
     # 连接超时时间（毫秒）
     timeout: 0
     
   ```

3. Config

   ```java
   @Configuration
   public class RedisConfig {
       @Bean
       public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
           RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
           redisTemplate.setConnectionFactory(redisConnectionFactory);
   
           // 使用Jackson2JsonRedisSerialize  需要导入依赖 com.fasterxml.jackson.core jackson-databind
           Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
   
           ObjectMapper objectMapper = new ObjectMapper();
           // 第一个参数表示: 表示所有访问者都受到影响 包括 字段, getter / isGetter,setter，creator
           // 第二个参数表示: 所有类型的访问修饰符都是可接受的，不论是公有还有私有表变量都会被序列化
           objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
           objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
           jackson2JsonRedisSerializer.setObjectMapper(objectMapper);
           // 设置key,value 序列化规则
           redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
           redisTemplate.setKeySerializer(new StringRedisSerializer());
           redisTemplate.afterPropertiesSet();
           return redisTemplate;
       }
   }
   ```

4. 使用 `RedisTemplate`操作Redis

   > 由于对目前项目代码`RedisTemplate`封装不是很满意，所以暂不贴出代码





### ElasticSearch

> 使用新的稳定版本：elasticsearch:7 、kibana:7

1 docker镜像拉取

```bash
docker pull elasticsearch:7.14.2
```

2 创建持久化文件夹

```bash
	sudo mkdir -p /env/dockerEnv/elasticsearch/config
	sudo mkdir -p /env/dockerEnv/elasticsearch/data
```

3 配置yml

```bash
echo "http.host: 0.0.0.0" >> /env/dockerEnv/elasticsearch/config/elasticsearch.yml
```

4 启动es容器

```bash
sudo docker run --name elasticsearch -p 9200:9200  -p 9300:9300 \
 -e "discovery.type=single-node" \
 -e ES_JAVA_OPTS="-Xms84m -Xmx512m" \
 -v /env/dockerEnv/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
 -v /env/dockerEnv/elasticsearch/data:/usr/share/elasticsearch/data \
 -v /env/dockerEnv/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
 -d elasticsearch:7.14.2
 
 # 以下是解释，不是命令！！！
 -p 端口映射
 -e discovery.type=single-node 单点模式启动
 -e ES_JAVA_OPTS="-Xms84m -Xmx512m"：设置启动占用的内存范围
 -v 目录挂载
 -d 后台运行
```

5 查看运行情况、启动日志

```bash
 #查看docker运行的容器
 docker ps
 #查看日志
 docker logs elasticsearch
```

发现log启动一定会报错..

```verilog
"stacktrace": ["org.elasticsearch.bootstrap.StartupException: ElasticsearchException[failed to bind service]; nested: AccessDeniedException[/usr/share/elasticsearch/data/nodes];",
"at org.elasticsearch.bootstrap.Elasticsearch.init(Elasticsearch.java:163) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Elasticsearch.execute(Elasticsearch.java:150) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.cli.EnvironmentAwareCommand.execute(EnvironmentAwareCommand.java:75) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.cli.Command.mainWithoutErrorHandling(Command.java:116) ~[elasticsearch-cli-7.14.2.jar:7.14.2]",
"at org.elasticsearch.cli.Command.main(Command.java:79) ~[elasticsearch-cli-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Elasticsearch.main(Elasticsearch.java:115) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Elasticsearch.main(Elasticsearch.java:81) ~[elasticsearch-7.14.2.jar:7.14.2]",
"Caused by: org.elasticsearch.ElasticsearchException: failed to bind service",
"at org.elasticsearch.node.Node.<init>(Node.java:798) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.node.Node.<init>(Node.java:281) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Bootstrap$5.<init>(Bootstrap.java:219) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Bootstrap.setup(Bootstrap.java:219) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Bootstrap.init(Bootstrap.java:399) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Elasticsearch.init(Elasticsearch.java:159) ~[elasticsearch-7.14.2.jar:7.14.2]",
"... 6 more",
"Caused by: java.nio.file.AccessDeniedException: /usr/share/elasticsearch/data/nodes",
"at sun.nio.fs.UnixException.translateToIOException(UnixException.java:90) ~[?:?]",
"at sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:106) ~[?:?]",
"at sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:111) ~[?:?]",
"at sun.nio.fs.UnixFileSystemProvider.createDirectory(UnixFileSystemProvider.java:396) ~[?:?]",
"at java.nio.file.Files.createDirectory(Files.java:694) ~[?:?]",
"at java.nio.file.Files.createAndCheckIsDirectory(Files.java:801) ~[?:?]",
"at java.nio.file.Files.createDirectories(Files.java:787) ~[?:?]",
"at org.elasticsearch.env.NodeEnvironment.lambda$new$0(NodeEnvironment.java:265) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.env.NodeEnvironment$NodeLock.<init>(NodeEnvironment.java:202) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.env.NodeEnvironment.<init>(NodeEnvironment.java:262) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.node.Node.<init>(Node.java:376) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.node.Node.<init>(Node.java:281) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Bootstrap$5.<init>(Bootstrap.java:219) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Bootstrap.setup(Bootstrap.java:219) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Bootstrap.init(Bootstrap.java:399) ~[elasticsearch-7.14.2.jar:7.14.2]",
"at org.elasticsearch.bootstrap.Elasticsearch.init(Elasticsearch.java:159) ~[elasticsearch-7.14.2.jar:7.14.2]",
"... 6 more"] }
uncaught exception in thread [main]
ElasticsearchException[failed to bind service]; nested: AccessDeniedException[/usr/share/elasticsearch/data/nodes];
Likely root cause: java.nio.file.AccessDeniedException: /usr/share/elasticsearch/data/nodes
        at java.base/sun.nio.fs.UnixException.translateToIOException(UnixException.java:90)
        at java.base/sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:106)
        at java.base/sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:111)
        at java.base/sun.nio.fs.UnixFileSystemProvider.createDirectory(UnixFileSystemProvider.java:396)
        at java.base/java.nio.file.Files.createDirectory(Files.java:694)
        at java.base/java.nio.file.Files.createAndCheckIsDirectory(Files.java:801)
        at java.base/java.nio.file.Files.createDirectories(Files.java:787)
        at org.elasticsearch.env.NodeEnvironment.lambda$new$0(NodeEnvironment.java:265)
        at org.elasticsearch.env.NodeEnvironment$NodeLock.<init>(NodeEnvironment.java:202)
        at org.elasticsearch.env.NodeEnvironment.<init>(NodeEnvironment.java:262)
        at org.elasticsearch.node.Node.<init>(Node.java:376)
        at org.elasticsearch.node.Node.<init>(Node.java:281)
        at org.elasticsearch.bootstrap.Bootstrap$5.<init>(Bootstrap.java:219)
        at org.elasticsearch.bootstrap.Bootstrap.setup(Bootstrap.java:219)
        at org.elasticsearch.bootstrap.Bootstrap.init(Bootstrap.java:399)
        at org.elasticsearch.bootstrap.Elasticsearch.init(Elasticsearch.java:159)
        at org.elasticsearch.bootstrap.Elasticsearch.execute(Elasticsearch.java:150)
        at org.elasticsearch.cli.EnvironmentAwareCommand.execute(EnvironmentAwareCommand.java:75)
        at org.elasticsearch.cli.Command.mainWithoutErrorHandling(Command.java:116)
        at org.elasticsearch.cli.Command.main(Command.java:79)
        at org.elasticsearch.bootstrap.Elasticsearch.main(Elasticsearch.java:115)
        at org.elasticsearch.bootstrap.Elasticsearch.main(Elasticsearch.java:81)
For complete error details, refer to the log at /usr/share/elasticsearch/logs/elasticsearch.log

```

解决

```bash
#切换到es目录
cd /env/dockerEnv/elasticsearch
#为所有用户所有组授予读、写、执行的权限 （所以下次安装可以提前执行这句话）
chmod -R 777 /env/dockerEnv/elasticsearch
#查看所有容器（包括未运行的）
docker ps -a
#显示如下
#CONTAINER ID   IMAGE                  COMMAND                  CREATED         STATUS          PORTS                                            NAMES
#97076006c5d3   elasticsearch:7.14.2   "/bin/tini -- /usr/l…"   8 minutes ago   Up 38 seconds   #0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp   elasticsearch

#利用容器ID进行启动
docker start 97076006c5d3
#查看运行的容器
docker ps
 #查看日志
 docker logs elasticsearch
#一不小心就启动成功了hh
```

查看集群信息（es会默认创建一个集群，不过目前只有一个节点）

```json
curl localhost:9200
{
  "name" : "97076006c5d3",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "3gRIb8V_RZ-V_qoq5Wn93w",
  "version" : {
    "number" : "7.14.2",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "6bc13727ce758c0e943c3c21653b3da82f627f75",
    "build_date" : "2021-09-15T10:18:09.722761972Z",
    "build_snapshot" : false,
    "lucene_version" : "8.9.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}

```



#### Kibana

拉取镜像

```
docker pull kibana:7.14.2
```

初始化配置

```bash
# 创建挂载配置目录
sudo mkdir -p /env/dockerEnv/kibana   
# 赋值读写执行权限
sudo chmod 777 /env/dockerEnv/kibana 
#yml配置
sudo echo "server.host: 0.0.0.0" >> /env/dockerEnv/kibana/kibana.yml
# 0.0.0.0 表示你的ip
sudo echo "elasticsearch.hosts: http://0.0.0.0:9200" >> /env/dockerEnv/kibana/kibana.yml
```

启动

```
sudo docker run --name kibana -v /env/dockerEnv/kibana/kibana.yml:/usr/share/kibana/config/kibana.yml -p 5601:5601 -d kibana:7.14.2
```







### Mysql

> [参考博客](https://juejin.cn/post/6844904036362092558)

1. 拉

   ```
   docker pull mysql:8.0
   ```

2. 运行

   ```bash
   docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=dictroot -d mysql:8.0
   # 查看日志
   docker logs mysql
   # 查看运行的容器
   dokcer ps
   ```

3. 开放3306端口，navicat测试连接失败

   >发现登录不了，报错：navicat不支持caching_sha_password加密方式
   >原因：mysql8.0使用新的密码加密方式：caching_sha_password
   >解决方式：修改成旧的加密方式（mysql_native_password），并重置密码

   服务器使用mysql命令行

   ```
   docker exec -it mysql bash
   mysql -uroot -p
   ```

   ```
   user mysql
   select host,user,plugin from user;
   alter user 'root'@'%' identified with mysql_native_password by 'yourpwd';
   ```

   

   

### Redis

> [参考博客](https://blog.csdn.net/qq_24958783/article/details/107541425)

1. 拉

```
 docker pull redis:latest
```

2. 创建外部目录用于挂载

   ```bash
   # 配置文件
   
   mkdir -p /env/dockerEnv/redis/conf
   
   # 数据文件
   
   mkdir -p /env/dockerEnv/redis/data
   
   # 创建conf文件
   
   touch  /env/dockerEnv/redis/conf/redis.conf
   ```

3. docker创建并启动redis

   ```bash
   docker run  -d --name redis -p 6379:6379 -v /env/dockerEnv/redis/data:/data -v /env/dockerEnv/redis/conf/redis.conf:/etc/redis/redis.conf   redis:latest 
   
   # 重启策略，这次暂不使用
   
   --restart unless-stopped \
   ```

4. 进入容器使用redis-cli，设置登录密码（这个配置不会持久化，重启容器会丢失。可以写在conf里）

   ```bash
        # 进入容器
     docker exec -it redis /bin/bash
        # redis客户端
        redis-cli
        # 随便执行一个命令，查看当前redis key大小
        dbsize
        # 获取密码
        config get requirepass
        # 设置密码
        config set requirepass yourpwd
        #退出cli
        exit
        #退出容器m，容器继续运行
        Ctrl+P和Ctrl+Q分别按
   ```

   





   >Docker容器的重启策略如下：
   >
   >no，默认策略，在容器退出时不重启容器
   >on-failure，在容器非正常退出时（退出状态非0），才会重启容器
   >on-failure:3，在容器非正常退出时重启容器，最多重启3次
   >always，在容器退出时总是重启容器
   >unless-stopped，在容器退出时总是重启容器，但是不考虑在Docker守护进程启动时就已经停止了的容器











---

*write something what you got, thanks for sharing*