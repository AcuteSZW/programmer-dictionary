import Vue from 'vue'
import Router from 'vue-router'
// import Login from '@/components/login'
// import Register from '@/components/register'
// import Manage from '@/components/manage'
// import FriendList from '@/components/friend/FriendList'
Vue.use(Router)

export default new Router({
    mode: "history",
    routes: [{
        path: '/',
        name: 'Login',
        component: resolve => require(['@/components/login'], resolve),
    }, {
        path: '/register',
        name: 'Register',
        component: resolve => require(['@/components/register'], resolve),
    }, {
        path: '/manage',
        name: 'Manage',
        component: resolve => require(['@/components/manage'], resolve),
        children: [{
            path: '/friendList',
            name: 'FriendList',
            component: resolve => require(['@/components/friend/friendList'], resolve),
        }, {
            path: '/bindFriend',
            name: 'BindFriend',
            component: resolve => require(['@/components/friend/bindFriend'], resolve),
        }, {
            path: '/messageBoard',
            name: 'MessageBoard',
            component: resolve => require(['@/components/friend/messageBoard'], resolve),
        }, {
            path: '/searchFriend',
            name: 'SearchFriend',
            component: resolve => require(['@/components/friend/searchFriend'], resolve),
        }, {
            path: '/addFriend',
            name: 'AddFriend',
            component: resolve => require(['@/components/friend/addFriend'], resolve),
        }, {
            path: '/dictManage',
            name: 'DictManage',
            component: resolve => require(['@/components/dictManage/dictManage'], resolve),
        }, {
            path: '/ascription',
            name: 'Ascription',
            component: resolve => require(['@/components/dictManage/ascription'], resolve),
        }, {
            path: '/member',
            name: 'Member',
            component: resolve => require(['@/components/dictManage/member'], resolve),
        }, {
            path: '/dollarPoint',
            name: 'DollarPoint',
            component: resolve => require(['@/components/amount/dollarPoint'], resolve),
        }, {
            path: '/topUpBalance',
            name: 'TopUpBalance',
            component: resolve => require(['@/components/amount/topUpBalance'], resolve),
        }, {
            path: '/accountBalance',
            name: 'AccountBalance',
            component: resolve => require(['@/components/amount/accountBalance'], resolve),
        }, {
            path: '/sellingThesaurus',
            name: 'SellingThesaurus',
            component: resolve => require(['@/components/public/sellingThesaurus'], resolve),
        }, {
            path: '/buyThesaurus',
            name: 'BuyThesaurus',
            component: resolve => require(['@/components/public/buyThesaurus'], resolve),
        }, {
            path: '/needThesaurus',
            name: 'NeedThesaurus',
            component: resolve => require(['@/components/public/needThesaurus'], resolve),
        }, {
            path: '/deleteThesaurus',
            name: 'DeleteThesaurus',
            component: resolve => require(['@/components/public/deleteThesaurus'], resolve),
        }, {
            path: '/usersReport',
            name: ' UsersReport',
            component: resolve => require(['@/components/badWords/usersReport'], resolve),
        }, {
            path: '/thesaurusReport',
            name: 'ThesaurusReport',
            component: resolve => require(['@/components/badWords/thesaurusReport'], resolve),
        }, {
            path: '/userThumbUp',
            name: 'UserThumbUp',
            component: resolve => require(['@/components/badWords/userThumbUp'], resolve),
        }, {
            path: '/thesaurusThumbUp',
            name: 'ThesaurusThumbUp',
            component: resolve => require(['@/components/badWords/thesaurusThumbUp'], resolve),
        }, {
            path: '/order',
            name: 'Order',
            component: resolve => require(['@/components/order/order'], resolve),
        }, {
            path: '/permission',
            name: 'Permission',
            component: resolve => require(['@/components/admin/permission'], resolve),
        }, {
            path: '/permissionsRecycle',
            name: 'PermissionsRecycle',
            component: resolve => require(['@/components/admin/permissionsRecycle'], resolve),
        }, {
            path: '/disableThesaurus',
            name: 'DisableThesaurus',
            component: resolve => require(['@/components/admin/disableThesaurus'], resolve),
        }, {
            path: '/unsealThesaurus',
            name: 'UnsealThesaurus',
            component: resolve => require(['@/components/admin/unsealThesaurus'], resolve),
        }, ]
    }]
})